/* ******************************************************************
 * Constantes de configuration
 * ****************************************************************** */
apiKey = "ce916043-3e7c-4351-a32e-1a7cbfdf57d1";      //"bb6b6c00-7303-49c1-aa34-9e3e557d67bf";

const serverUrl = "https://lifap5.univ-lyon1.fr";

/* ******************************************************************
 * Gestion de la boîte de dialogue (a.k.a. modal) d'affichage de
 * l'utilisateur.
 * ****************************************************************** */

/**
 * Fait une requête GET authentifiée sur /whoami
 * @returns une promesse du login utilisateur ou du message d'erreur
 */
function fetchWhoami() {
  return fetch(serverUrl + "/whoami", { headers: { "Api-Key": apiKey } })
    .then((response) => {
      if (response.status === 401) {
        return response.json().then((json) => {
          console.log(json);
          return { err: json.message };
        });
      } else {
        return response.json();
      }
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Requête GET sur le serveur pour récuperer les pokémons
 * @returns
 */
function fetchPokemon() {
  return fetch(serverUrl + "/pokemon")
    .then((response) => {
      if (response.status === 401) {
        return response.json().then((json) => {
          console.log(json);
          return { err: json.message };
        });
      } else {
        return response.json();
      }
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Affiche une modale qui demande une clé API a entrer par l'utilisateur puis,
 * fait une requête sur le serveur et insère un login à coté du bouton déconnexion,
 * tout en fermant la modale de connexion
 * @param {Etat} etatCourant l'état courant
 * @returns Une promesse de mise à jour
 */

 function lanceWhoamiEtInsereLogin(etatCourant) {

 majEtatEtPage(etatCourant, {
    loginModal: true,
    })

  const form = document.getElementById("form");
  form.addEventListener('submit', (e)=> {
    e.preventDefault();

    const password = document.getElementById("password").value;
      apiKey = password;
      return fetchWhoami().then((data) => {
        majEtatEtPage(etatCourant, {
          login: data.user, // qui vaut undefined en cas d'erreur
          errLogin: data.err, // qui vaut undefined si tout va bien
          loginModal: false, // on affiche la modale
        })
      });
  })
}

/**
 * Génère la barre de recherche pour chercher un pokémon
 * @param {Etat} etatCourant
 * @returns
 */
function genereRecherche(etatCourant) {
  const html = `
  <input type="search" id="barre-rs" style="font-size: 1rem; left: 50%"
  placeholder="Cherchez un pokémon" data-search>
  `;
  return {
    html: html,
    callbacks: {
      "barre-rs": {
      },
    },
  };
}



/**
 * Permet la récupération de la liste des pokémons sur le serveur,
 * et met à jour le champ listepokemon de l'etat Courant
 * @param {Etat} etatCourant
 * @returns
 */
function lanceFetchPokemon(etatCourant) {
  return fetchPokemon().then((data) => {
    majEtatEtPage(etatCourant, {
      listepokemon: data,
    });
  });
}

/**
 * Génère le code HTML du corps de la modale de login. On renvoie en plus un
 * objet callbacks vide pour faire comme les autres fonctions de génération,
 * mais ce n'est pas obligatoire ici.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et un objet vide
 * dans le champ callbacks
 */
function genereModaleLoginBody(etatCourant) {
  const text =
    etatCourant.errLogin !== undefined
      ? etatCourant.errLogin
      : etatCourant.login;

  //if (etatCourant.login == undefined) {
  return {
  html: `
  <section class="modal-card-body">
    <div>
      <label for="password">Clé d'API</label><br>
      <input id="password" name="password" type="password" style="font-size: 1.5rem;">
    </div>
  </section>
  `
  };
}

/**
 * Génère le code HTML du titre de la modale de login et les callbacks associés.
 *
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereModaleLoginHeader(etatCourant) {
  return {
    html: `
<header class="modal-card-head  is-back">
  <p class="modal-card-title">Utilisateur</p>
  <button
    id="btn-close-login-modal1"
    class="delete"
    aria-label="close"
    ></button>
</header>`,
    callbacks: {
      "btn-close-login-modal1": {
        onclick: () => majEtatEtPage(etatCourant, { loginModal: false }),
      },
    },
  };
}

/**
 * Génère le code HTML du base de page de la modale de login et les callbacks associés.
 * La modale demande uniquement de entrer une clé API
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereModaleLoginFooter(etatCourant) {
  return {
    html: `
  <form id="form" class="modal-card-foot" style="justify-content: flex-end">
    <button type="submit" id="login" class="is-success button">Valider</button>
    <button id="btn-close-login-modal2" class="button">Fermer</button>
  </form>
  `,
    callbacks: {
      "btn-close-login-modal2": {
        onclick: () => majEtatEtPage(etatCourant, { loginModal: false }),
      },
    },
  };
}

/**
 * Génère le code HTML de la modale de login et les callbacks associés.
 *
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereModaleLogin(etatCourant) {
  const header = genereModaleLoginHeader(etatCourant);
  const footer = genereModaleLoginFooter(etatCourant);
  const body = genereModaleLoginBody(etatCourant);
  const activeClass = etatCourant.loginModal ? "is-active" : "is-inactive";
  return {
    html: `
      <div id="mdl-login" class="modal ${activeClass}">
        <div class="modal-background"></div>
        <div class="modal-card">
          ${header.html}
          ${body.html}
          ${footer.html}
        </div>
      </div>`,
    callbacks: { ...header.callbacks, ...footer.callbacks, ...body.callbacks },
  };
}

/* ************************************************************************
 * Gestion de barre de navigation contenant en particulier les bouton Pokedex,
 * Combat et Connexion.
 * ****************************************************************** */

/**
 * Déclenche la mise à jour de la page en changeant l'état courant pour que la
 * modale de login soit affichée
 * @param {Etat} etatCourant
 */
function afficheModaleConnexion(etatCourant) {
  lanceWhoamiEtInsereLogin(etatCourant);
}

/**
 * Génère le code HTML et les callbacks pour la partie droite de la barre de
 * navigation qui contient le bouton de login.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereBoutonConnexion(etatCourant) {
  const html = `
  <div class="navbar-end">
    <div class="navbar-item">
      <div class="buttons"style="position: relative; left: 100%">
        <a id="btn-open-login-modal" class="is-success button" style="position: relative; left: 100%"> Connexion </a>
      </div>
    </div>
  </div>`;
  return {
    html: html,
    callbacks: {
      "btn-open-login-modal": {
        onclick: () => afficheModaleConnexion(etatCourant),
      },
    },
  };
}

/**
 * Fonction qui permet la déconnexion, elle est liée
 * au bouton déconnexion généré après un login
 * @param {Etat} etatCourant
 */
 function Deco(etatCourant) {
  apiKey = "ce916043-3e7c-4351-a32e-1a7cbfdf57d1";
  {majEtatEtPage(etatCourant, {
    login: undefined,
    errLogin: undefined,
    loginModal: false,
   })
  }
}

/**
 * Génère le code HTML et les callbacks pour la partie droite de la barre de
 * navigation qui contient cette fois le bouton de deconnexion lorsqu'une clé API
 * a été entrée.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereBoutonDeco(etatCourant) {
  const html = `
  <div class="navbar-end">
    <div class="navbar-item">
      <div class="buttons">
        <a id="btn-deco-modal" class="is-danger button"> Déconnexion </a>
      </div>
    </div>
  </div>`;
  return {
    html: html,
    callbacks: {
      "btn-deco-modal": {
        onclick: () => Deco(etatCourant),
      },
    }
  };
}

/**
 * Fonction permettant la gestion de l'affichage des pokémons.
 * Ici, elle permet d'afficher les 10 prochains pokémons
 * @param {Etat} etatCourant
 */
 function AfficherPlus (etatCourant) {
  etatCourant.nbPoke+10 < etatCourant.listepokemon.length ?
  majEtatEtPage(etatCourant, {
    nbPoke: etatCourant.nbPoke+10,
  }) : console.log("Limite tab");
}

/**
 * Fonction permettant la gestion de l'affichage des pokémons.
 * Ici, elle permet de cacher les 10 derniers pokémons
 * @param {Etat} etatCourant
 */
function AfficherMoins(etatCourant) {
  etatCourant.nbPoke > 10 ? majEtatEtPage(etatCourant, {
    nbPoke: etatCourant.nbPoke-10,
  }) : console.log("Limite basse pokemons");
}

/**
 * Génère les boutons pour afficher / cacher les pokémons de la liste
 * @param {Etat} etatCourant
 * @returns
 */
function genereBoutonAff(etatCourant) {
  const html = `
  <div class="button-is-centered" style="position: relative; left: 50%">
    <button id="AffPlus" class="button">Plus</button>
    <button id="AffMoins" class="button">Moins</button>
  </div>`
  return {
  html: html,
  callbacks: {
    "AffPlus": {
        onclick: () => AfficherPlus(etatCourant),
      },
    "AffMoins": {
        onclick: () => AfficherMoins(etatCourant),
      },
    }
  };
}

/**
 * Génère le code HTML de la barre de navigation et les callbacks associés.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereBarreNavigation(etatCourant) {
  const deco = genereBoutonDeco(etatCourant);
  const connexion = genereBoutonConnexion(etatCourant);
  const barre = genereRecherche(etatCourant);
  if (etatCourant.login != undefined) {
    return {
      html: `
      <div id="root">
        <nav class="navbar" role="navigation" aria-label="main navigation">
          <div class="navbar">
          ${barre.html}
            <div class="navbar-item"><div class="buttons">
                <a id="btn-pokedex" class="button is-light"> Pokedex </a>
                  <a id="btn-combat" class="button is-light"> Combat </a>
            </div>
          </div>
        ${deco.html}
        <div class="buttons">
        <class="button is-light" style="background-color:white;">${etatCourant.login}
        </div>
        </nav>
      </div>
    `,
      callbacks: {
        ...deco.callbacks,
        "btn-pokedex": { onclick: () => console.log("click bouton pokedex") },
      },
    };
  }

  else {
    return {
    html: `
    <div id="root">
      <nav class="navbar" role="navigation" aria-label="main navigation">
        <div class="navbar">
        ${barre.html}
          <div class="navbar-item"><div class="buttons">
              <a id="btn-pokedex" class="button is-light"> Pokedex </a>
                <a id="btn-combat" class="button is-light"> Combat </a>
          </div>
        </div>
      ${connexion.html}
      </div>
      </nav>
    </div>
  `,
      callbacks: {
        ...connexion.callbacks,
        "btn-pokedex": { onclick: () => console.log("click bouton pokedex") },
      },
    };
  }
}

/**
 * Génère la liste des pokémons récupérés par le fetchPokémon
 * (mise en forme du contenu de la liste)
 * @param {Etat} etatCourant
 * @returns
 */
 function genereListePokemon(etatCourant){
   const html = {};
   html.liste = ``;
   etatCourant.listepokemon.slice(0,etatCourant.nbPoke).forEach((pokemon, indice) => {
       html.liste += `
           <tr class="listepokemon">
             <td>
               <img
                 alt="${pokemon.Name}"
                 src=${pokemon.Images.Detail}
                 width="64"
               />
             </td>
             <td>${indice+1}</td>
             <td>${pokemon.Name}</td>
             <td>
               <ul>
               `
     pokemon.Abilities.forEach((ability) => {html.liste += `<li class = "li">${ability}</li>`;});
     html.liste +=
             `</ul>
           </td>
           <td>
             <ul>
             `
     pokemon.Types.forEach((type) => {html.liste += `<li class = "li">${type}</li>`;});

     html.liste +=
             `
             </ul>
           </td>
         </tr>
     `;
   });

   return {
     html:html.liste,
     callbacks:{
         "tr": { onclick: (indice) => {
           majEtatEtPage(etatCourant,{indiceSelected:indice});
           document.getElementById('tbd').getElementsByTagName('tr')[indice].className = "is-selected";
       } },
     }
   };
 }
/**
 * Génère les entêtes de la liste des pokémons
 * @param {Etat} etatCourant
 * @returns
 */
 function genereTitreListePokemon(etatCourant){
   return {
     html:`
       <thead id = "thd">
         <tr>
           <th><span>Image</span>
             <span class="icon">
               <i class="">
               </i>
             </span>
           </th>
           <th><span>#</span>
           <span class="icon">
             <i class="">
             </i>
           </span>
           </th>
           <th><span>Name</span>
           <span class="icon">
             <i class="">
             </i>
           </span></th>
           <th><span>Abilities</span>
           <span class="icon">
             <i class="">
             </i>
           </span></th>
           <th><span>Types</span>
           <span class="icon">
             <i class="">
             </i>
           </span></th>
         </tr>
       </thead>
     `,
     callbacks:{
       "th":{onclick: (indice) => sort(indice, etatCourant)},
     }
   };
 }

 function getBodyList(trs){

   return trs.map(tr => {
     //transmtre les tds à tableau
     const tds = Array.from(tr.getElementsByTagName('td'));
     //transmtre les contenues des td à entier
     const tdsV = tds.map((td, index) => {
       if(index <= 1){
         return parseInt(td.innerHTML);
       }else if(index == 2){
         return td.innerHTML;
       }else{
         return td.getElementsByClassName('li')[0].innerHTML;
       }
     });
     return {
       tr: tr,
       tdsV: tdsV
     }

   });
 }

/**
 * Génère toute la partie gauche du site, à savoir
 * la liste des pokémons ainsi que les boutons liés à leur affichage
 * @param {Etat} etatCourant
 * @returns
 */
 function genereContenuGauche(etatCourant){
   const titre = genereTitreListePokemon(etatCourant);
   const liste = genereListePokemon(etatCourant);
   const btn = genereBoutonAff(etatCourant);
   const contenuGauche =`
   <div class="tabs is-centered">
     <ul>
       <li class="is-active" id="tab-all-pokemons">
         <a>Tous les pokemons</a>
       </li>
       <li id="tab-tout"><a>Mes pokemons</a></li>
     </ul>
   </div>
       <div id="tbl-pokemons">

         <table class="table">
             ${titre.html}
           <tbody id = "tbd">
             ${liste.html}
           </tbody>
         </table>
           ${btn.html}
       </div>
       `

   return{
     html:contenuGauche,
     callbacks:{...titre.callbacks, ...liste.callbacks, ...btn.callbacks},
   };
 }

/**
 * Génère l'affichage des détails du pokémon sélectionné
 * @param {Etat} etatCourant
 * @returns
 */

 function genereContenuDroite(etatCourant){
   const html = {};
   const indice = etatCourant.indiceSelected;
   const pokemon = etatCourant.listepokemon[indice];
   const japaneseName = pokemon.JapaneseName;
   html.abilities = ``;
   html.againstR = ``;
   html.againstW = ``;
   pokemon.Abilities.forEach((ability) => {html.abilities += `<li>${ability}</li>`;});
   Object.entries(pokemon.Against).forEach((value) => {
     if(value[1] >= 1) html.againstR += `<li>${value[0]}</li>`
     else html.againstW += `<li>${value[0]}</li>`
   });
   html.card = `
     <div class="card">
       <div class="card-header">
         <div class="card-header-title">${japaneseName} (#${indice+1})</div>
       </div>
       <div class="card-content">
         <article class="media">
           <div class="media-content">
             <h1 class="title">${pokemon.Name}</h1>
           </div>
         </article>
       </div>
       <div class="card-content">
         <article class="media">
           <div class="media-content">
             <div class="content has-text-left">
               <p>Hit points: ${pokemon.Attack}</p>
               <h3>Abilities</h3>
               <ul>
                 ${html.abilities}
               </ul>
               <h3>Resistant against</h3>
               <ul>
                 ${html.againstR}
               </ul>
               <h3>Weak against</h3>
               <ul>
                 ${html.againstW}
               </ul>
             </div>
           </div>
           <figure class="media-right">
             <figure class="image is-475x475">
               <img
                 class=""
                 src="${pokemon.Images.Full}"
                 alt="Bulbasaur"
               />
             </figure>
           </figure>
         </article>
       </div>
       <div class="card-footer">
         <article class="media">
           <div class="media-content">
             <button class="is-success button" tabindex="0">
               Ajouter à mon deck
             </button>
           </div>
         </article>
       </div>
     </div>
   `
   return{
     html:html.card,
   };
 }

/**
 * Affiche les deux sections droites et gauche du site,
 * à savoir la liste à gauche et les détails des pokémons à droite
 * @param {Etat} etatCourant
 * @returns
 */
function genereSection(etatCourant){
  const gauche = genereContenuGauche(etatCourant);
  const droite = genereContenuDroite(etatCourant);
  return{
    html:`
    <section class="section">
      <div class="columns">
        <div class="column">
          ${gauche.html}
        </div>
        <div class="column">
          ${droite.html}
        </div>
      </div>
    </section>
    `,
    callbacks: {...gauche.callbacks, ...droite.callbacks},
  };

}
/**
 * Génére le code HTML de la page ainsi que l'ensemble des callbacks à
 * enregistrer sur les éléments de cette page.
 *
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function generePage(etatCourant) {
  const barredeNavigation = genereBarreNavigation(etatCourant);
  const modaleLogin = genereModaleLogin(etatCourant);
  const section = genereSection(etatCourant);
  const generebarre = genereBoutonConnexion(etatCourant);
  // remarquer l'usage de la notation ... ci-dessous qui permet de "fusionner"
  // les dictionnaires de callbacks qui viennent de la barre et de la modale.
  // Attention, les callbacks définis dans modaleLogin.callbacks vont écraser
  // ceux définis sur les mêmes éléments dans barredeNavigation.callbacks. En
  // pratique ce cas ne doit pas se produire car barreDeNavigation et
  // modaleLogin portent sur des zone différentes de la page et n'ont pas
  // d'éléments en commun.
  return {
    html: barredeNavigation.html + modaleLogin.html + section.html + generebarre.html,
    callbacks: { ...barredeNavigation.callbacks, ...modaleLogin.callbacks, ...section.callbacks },
  };
}

/* ******************************************************************
 * Initialisation de la page et fonction de mise à jour
 * globale de la page.
 * ****************************************************************** */

/**
 * Créée un nouvel état basé sur les champs de l'ancien état, mais en prenant en
 * compte les nouvelles valeurs indiquées dans champsMisAJour, puis déclenche la
 * mise à jour de la page et des événements avec le nouvel état.
 *
 * @param {Etat} etatCourant etat avant la mise à jour
 * @param {*} champsMisAJour objet contenant les champs à mettre à jour, ainsi
 * que leur (nouvelle) valeur.
 */
function majEtatEtPage(etatCourant, champsMisAJour) {
  const nouvelEtat = { ...etatCourant, ...champsMisAJour };
  majPage(nouvelEtat);
}

/**
 * Prend une structure décrivant les callbacks à enregistrer et effectue les
 * affectation sur les bon champs "on...". Par exemple si callbacks contient la
 * structure suivante où f1, f2 et f3 sont des callbacks:
 *
 * { "btn-pokedex": { "onclick": f1 },
 *   "input-search": { "onchange": f2,
 *                     "oninput": f3 }
 * }
 *
 * alors cette fonction rangera f1 dans le champ "onclick" de l'élément dont
 * l'id est "btn-pokedex", rangera f2 dans le champ "onchange" de l'élément dont
 * l'id est "input-search" et rangera f3 dans le champ "oninput" de ce même
 * élément. Cela aura, entre autres, pour effet de délclencher un appel à f1
 * lorsque l'on cliquera sur le bouton "btn-pokedex".
 *
 * @param {Object} callbacks dictionnaire associant les id d'éléments à un
 * dictionnaire qui associe des champs "on..." aux callbacks désirés.
 */
 function enregistreCallbacks(callbacks) {
   Object.keys(callbacks).forEach((id) => {
     const elt = document.getElementById(id);
     if (elt === undefined || elt === null) {
       const eltC = document.getElementById('tbd').getElementsByTagName(id);
       if(eltC.length == 0){
         const eltT = document.getElementById('thd').getElementsByTagName(id);
         if(eltT === undefined || eltT === null){
           console.log(
             `Élément inconnu: ${id}, impossible d'enregistrer de callback sur cet id`
           );
         }else{
           const tab = Array.from(eltT);
           Object.keys(callbacks[id]).forEach((onAction) => {
             tab.forEach((element,key) => {
               eltT[key][onAction] = () => callbacks[id][onAction](key);
             });
           });
         }

       } else {
         const tab = Array.from(eltC);
         Object.keys(callbacks[id]).forEach((onAction) => {
           tab.forEach((element,key) => {
             eltC[key][onAction] = () => callbacks[id][onAction](key);
           });

         });
       }

     } else {
       Object.keys(callbacks[id]).forEach((onAction) => {
         elt[onAction] = callbacks[id][onAction];
       });
     }
   });
 }
/**
 * Mets à jour la page (contenu et événements) en fonction d'un nouvel état.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majPage(etatCourant) {
  console.log("CALL majPage");
  const page = generePage(etatCourant);
  document.body.innerHTML = page.html;
  enregistreCallbacks(page.callbacks);
}

/**
 * Appelé après le chargement de la page.
 * Met en place la mécanique de gestion des événements
 * en lançant la mise à jour de la page à partir d'un état initial.
 */
function initClientPokemons() {
  console.log("CALL initClientPokemons");
  const etatInitial = {
    loginModal: false,
    login: undefined,
    listepokemon: undefined,
    errLogin: undefined,
    indiceSelected:0,
    nbPoke: 10,
  };
  //majPage(etatInitial);
  lanceFetchPokemon(etatInitial);
}

// Appel de la fonction init_client_duels au après chargement de la page
document.addEventListener("DOMContentLoaded", () => {
  console.log("Exécution du code après chargement de la page");
  initClientPokemons();
});
